import React from 'react';
import HeroCards from '../../Components/HeroCards';
import Pagination from '../../Components/Pagination';
import Loader from '../../Components/Loader';


export default class Home extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      isLoading: false,
      dataHero: [],
      pageSize: 24,
      totalData: 179,
      currentPage: 1,
    };
  }

  componentDidMount() {
    this.fetchHero(this.state.pageSize, 0);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  fetchHero = async (limit = 179, skip = 0) => {
    this.setState({ isLoading: true });
    try {
      const config = {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'X-Parse-Application-Id': 'Y4tdEGq1F5TKchVR5I4k6MiF9LUVg5tEVHJNRTQF',
          'X-Parse-REST-API-Key': 'uzTOzVAW2PnfN6RpeY8jTGfGwz8dbRKHd65CBar1'
        }
      };
      const url = `https://parseapi.back4app.com/classes/hero?limit=${limit}&skip=${skip}`;
      const request = await fetch(url, config);
      const response = await request.json();
      if (response && response.results) {
        this.setState({
          isLoading: false,
          dataHero: response.results
        });
      } else {
        this.setState({ isLoading: false });
      }
    } catch (error) {
      this.setState({ isLoading: false });
      console.log('error', error);
    }
  }
  onChangePage = (page) => {
    const { pageSize } = this.state
    const offset = (page - 1) * pageSize;
    this.fetchHero(pageSize, offset);
    this.setState({
      currentPage: page,
    })
  }


  render() {
    const { dataHero, isLoading, pageSize, totalData, currentPage } = this.state;
    return (
      <div>
        {isLoading &&
          <Loader />
        }
        {dataHero && dataHero.length > 0 &&
          <HeroCards data={dataHero} />
        }
        {dataHero && dataHero.length > 0 &&
          <Pagination onChangePage={this.onChangePage} initialPage={currentPage} pageSize={pageSize} totalData={totalData} />
        }
      </div>
    );
  }
}