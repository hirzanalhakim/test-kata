import React from 'react';
import {
  Navbar,
  NavbarBrand
} from 'reactstrap';

export default class Header extends React.PureComponent {
  render() {
    return (
      <Navbar color="dark" dark expand="md">
        <NavbarBrand href="#">Kata.ai</NavbarBrand>
      </Navbar>
    )
  }
}