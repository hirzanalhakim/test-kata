import React from 'react';
import { Container, Row } from 'reactstrap';
import HeroCard from '../HeroCard';
import HeroDetailsModal from '../HeroDetailsModal';

export default class HeroCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      clickCard: null
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle = (data) => {
    this.setState({
      clickCard: data,
      modal: !this.state.modal
    });
  }
  render() {
    const { modal, clickCard } = this.state;
    const { data } = this.props;
    return (
      <Container style={{ marginTop: '20px' }}>
        <Row>
          {data && data.length > 0 && data.map((data, index) => (
            <HeroCard key={index} toggle={() => this.toggle(data)} data={data} />
          ))
          }
        </Row>
        {clickCard &&
          <HeroDetailsModal toggle={this.toggle} isOpen={modal} data={clickCard} />
        }
      </Container>
    )
  }
}