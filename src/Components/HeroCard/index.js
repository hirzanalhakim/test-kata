import React from 'react';
import { Card, CardImg, Col } from 'reactstrap';
import PropTypes from 'prop-types';

export default class HeroCard extends React.PureComponent {
  render() {
    const { data, toggle } = this.props;
    return (
      <Col xs="6" md="3">
        <Card style={{ marginBottom: '15px' }} className="card-container" onClick={toggle}>
          <div className="overlay">
            <div className="card-title">
              {data && data.name}
              <div style={{ marginTop: '10px', textAlign: 'left', fontSize: '12px', paddingLeft: '5px' }}>
                <p className="detail-overlay">Bord: {data && data.born}</p>
                <p className="detail-overlay">Died: {data && data.died}</p>
              </div>

            </div>
          </div>
          <CardImg top
            src={data && data.img}
            alt='Judul title'
            className="cover-image" 
            onError={event => {
              event.target.src = 'https://screenshotlayer.com/images/assets/placeholder.png';
            }}
          />
        </Card>
      </Col>
    )
  }
}

HeroCard.propTypes = {
  data: PropTypes.object.isRequired,
  toggle: PropTypes.func.isRequired,
}