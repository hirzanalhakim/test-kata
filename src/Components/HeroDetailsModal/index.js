import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row } from 'reactstrap';
import PropTypes from 'prop-types';

export default class HeroDetailsModal extends React.PureComponent {
  render() {
    const { data, isOpen, toggle } = this.props;
    return (
      <div>
        <Modal isOpen={isOpen}>
          <ModalHeader toggle={toggle}>Detail Hero</ModalHeader>
          <ModalBody>
            <Row>
              <Col xs="12" md="6">
                <img src={data && data.img}
                  alt='Judul title'
                  className="cover-image"
                  onError={event => {
                    event.target.src = 'https://screenshotlayer.com/images/assets/placeholder.png';
                  }} />
              </Col>
              <Col xs="12" md="6">
                <p>Name: {data && data.name}</p>
                <p>Born: {data && data.born}</p>
                <p>Died: {data && data.died}</p>
                <p>Notes: {data && data.notes}</p>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={toggle}>Ok</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}

HeroDetailsModal.propTypes = {
  data: PropTypes.object.isRequired,
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
}