import React from 'react';
import { Pagination as Paginate, PaginationItem, PaginationLink } from 'reactstrap';
import PropTypes from 'prop-types';

export default class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = { pager: {} };
  }


  componentWillMount() {
    if (this.props.initialPage) {
      this.setPage(this.props.initialPage);
    }
  }

  setPage(page) {
    const { pageSize, totalData } = this.props;
    let pager = this.state.pager;

    if (page < 1 || page > pager.totalPages) {
      return;
    }

    // get new pager object for specified page
    pager = this.getPager(totalData, page, pageSize);

    this.setState({ pager: pager });
  }

  changePage(page) {

    const { pageSize, totalData } = this.props;
    let pager = this.state.pager;
    if (page < 1 || page > pager.totalPages) {
      return;
    }

    pager = this.getPager(totalData, page, pageSize);

    this.setState({ pager: pager });

    this.props.onChangePage(page);
  }

  getPager(totalItems, currentPage, pageSize) {
    const totalPages = Math.ceil(totalItems / pageSize);
    let startPage, endPage;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // create an array of pages to pager control
    const pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      pages: pages
    };
  }

  render() {
    const { pager } = this.state;

    if (!pager.pages || pager.pages.length <= 1) {
      // don't display pager if there is only 1 page
      return null;
    }

    return (

      <Paginate aria-label="Page navigation example" style={{ marginTop: '40px', marginBottom: '30px', display: 'inline-flex' }}>
        <PaginationItem disabled={pager.currentPage === 1 ? true : false}>
          <PaginationLink onClick={() => this.changePage(1)}>
            First
          </PaginationLink>
        </PaginationItem>
        <PaginationItem disabled={pager.currentPage === 1 ? true : false} >
          <PaginationLink onClick={() => this.changePage(pager.currentPage - 1)}>
            Previous
          </PaginationLink>
        </PaginationItem>

        {pager.pages.map((page, index) =>
          <PaginationItem key={index} active={pager.currentPage === page ? true : false} >
            <PaginationLink onClick={() => this.changePage(page)}>
              {page}
            </PaginationLink>
          </PaginationItem>
        )}

        <PaginationItem>
          <PaginationLink onClick={() => this.changePage(pager.currentPage + 1)}>
            Next
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={() => this.changePage(pager.totalPages)}>
            Last
          </PaginationLink>
        </PaginationItem>
      </Paginate>

    );
  }
}

Pagination.propTypes = {
  initialPage: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  totalData: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
}